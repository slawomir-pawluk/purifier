# Purifier logger

Source: https://bitbucket.org/slawomir-pawluk/purifier

Shared volume: `/home/node/app/config`

Config structure:

``` javascript
// config.js

var config = {};

config.purifier = {
  address: "[Purifier IP]"
}

config.influx = {
  address: "[Influx IP]",
  databaseName: "[database name]"
}

module.exports = config;
```

## Running

Without docker managed volume

``` shell
docker run -v /local/path/to/config:/home/node/app/config --name purifier -d spawluk/purifier
```

With docker managed volume

``` shell
docker volume create --name purifier_config

docker run -v purifier_config:/home/node/app/config --name purifier -d spawluk/purifier
```