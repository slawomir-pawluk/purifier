'use strict';

var miio = require("miio");
var Influx = require("influx");
var Logger = require("./logger");
var config = require("./config/config");

const log = new Logger();

const influx = new Influx.InfluxDB({
  host: config.influx.address,
  database: config.influx.databaseName,
  schema: [
    {
      "measurement": "air_quality",
      fields: {
        temperature: Influx.FieldType.FLOAT,
        humidity: Influx.FieldType.INTEGER,
        aqi: Influx.FieldType.INTEGER
      },
      tags: []
    }
  ]
});

log.info("Connecting to device", config.purifier.address);

setInterval(() => {
  miio.device({ address: config.purifier.address })
    .then(device => {
      log.info(`Connected to device ${config.purifier.address}`);

      logParameters(device);

      device.destroy();
    })
    .catch(err => log.error(err));
}, 5000);

function logParameters(device) {
  if (device.property("power") === false) {
    log.info("Device is turned off");
    return;
  }

  log.info("Writing data to influx", device.properties);

  var temperature = device.property("temperature");
  var humidity = device.property("humidity");
  var aqi = device.property("aqi");

  influx.writeMeasurement("air_quality", [
    {
      fields: {
        aqi,
        temperature,
        humidity
      }
    }
  ]).catch(err => {
    log.error(`Error saving data to InfluxDB! ${err.stack}`)
  })
}