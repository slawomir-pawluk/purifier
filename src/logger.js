function getDate() {
  return new Date().toISOString().replace("T", " ").substring(0, 19);
}

class Logger {
  info(msg, obj) {
    if (obj) {
      console.log(`${getDate()} INFO - ${msg}`, obj);
    } else {
      console.log(`${getDate()} INFO - ${msg}`);
    }
  }

  error(msg) {
    console.log(`${getDate()} ERROR - ${msg}`);
  }
}

module.exports = Logger;